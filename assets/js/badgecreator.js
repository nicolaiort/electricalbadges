import {makeBadge} from '../libs/badgelib/index';
import {svg2base64, escapeXml} from '../libs/badgelib/svg-helpers';
import {remote, clipboard} from 'electron';
import $ from 'cash-dom';
import * as fs from 'fs';

const win = remote.getCurrentWindow();
const dialog = remote.dialog;

let svg;
let format = {
    label: 'label',
    message: 'message',
    labelColor: 'gray',
    color: 'green',
    style: 'for-the-badge',
    logo: ''
};
let dialogOptions = {
    title: "Save Badge",
    defaultPath: "change.me",
    buttonLabel: "Save Badge",
    filters: [
		{
            name: 'Vector',
            extensions: ['svg']
        }
    ]
};

function updateBadge() {
    svg = makeBadge(format);
    $('#svgcontainer').html(svg);
}

function saveBadge() {
	dialogOptions.defaultPath=`${format.label.replace(" ", "_")}-${format.message.replace(" ", "_")}-${format.labelColor}_${format.color}.svg`
	dialog.showSaveDialog(dialogOptions).then(file => { 
		console.log(file);
        if (!file.canceled) {               
            fs.writeFile(file.filePath.toString(),svg, function (err) { 
				if (err){
					M.toast({html: '<span style="background-color: red">Error while saveing the badge!</span>', displayLength: 2000});
					throw err;
				} 
                M.toast({html: 'Saved badge!', displayLength: 1000});
            }); 
        } 
    }).catch(err => { 
        console.log(err) 
    }); 
}

function addBadgeTriggers() {
    $('#label-input').on('input propertychange', function () {
		format.label = this.value;
		updateBadge();
	});

	$('#message-input').on('input propertychange', function () {
		format.message = this.value;
		updateBadge();
	});

	$('#label-color').on('change', function () {
		format.labelColor = this.value;
		updateBadge();
	});

	$('#message-color').on('change', function () {
		format.color = this.value;
		updateBadge();
	});

    $('#save-badge').on("click", () =>{
        saveBadge();
    })

    $('#copy-svg').on('click', () => {
        clipboard.writeText(svg);
        M.toast({html: 'Copied svg string to clipboard.', displayLength: 1000});
    });

    $('#copy-img').on('click', () => {
        const img = `<image src="${
            escapeXml(svg2base64(svg))
        }"/>`;
        clipboard.writeText(img);
        M.toast({html: 'Copied image tag to clipboard.', displayLength: 1000});
    });
}

module.exports = {
    updateBadge,
    addBadgeTriggers
};
