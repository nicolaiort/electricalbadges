import {remote} from 'electron';
const win = remote.getCurrentWindow();


function addTitlebarTriggers() {
	document.getElementById('min-button').addEventListener("click", event => {
        win.minimize();
    });
    
    document.getElementById('max-button').addEventListener("click", event => {
        win.maximize();
    });
    
    document.getElementById('restore-button').addEventListener("click", event => {
        win.unmaximize();
    });
    
    document.getElementById('close-button').addEventListener("click", event => {
        win.close();
    });
    
    win.on('maximize', toggleMaxRestoreButtons);
        win.on('unmaximize', toggleMaxRestoreButtons);
    
        function toggleMaxRestoreButtons() {
            if (win.isMaximized()) {
                document.body.classList.add('maximized');
            } else {
                document.body.classList.remove('maximized');
            }
        }
}

module.exports = {
	addTitlebarTriggers
};
