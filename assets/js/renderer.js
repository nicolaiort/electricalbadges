import $ from 'cash-dom';
import M from 'materialize-css';
import { updateBadge, addBadgeTriggers } from './badgecreator';
import { addTitlebarTriggers } from './titlebar';

$(() => {
	M.AutoInit();
	addTitlebarTriggers();
	addBadgeTriggers();
	updateBadge();
});
