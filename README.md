# Electrical⚡Badges

A offline capable badge creator.

![badge: based on electron](./.assets/Based_on-Electron-gray_blue.svg)
![badge: uses badges ](./.assets/uses-badges-blue_green.svg)

Powered by [electron](https://www.electronjs.org/) and based on the awesome work of the [shields project](https://github.com/badges/shields).<br>
Featuring styling by [bootstrap](https://getbootstrap.com/), [materialize](https://materializecss.com/) and [line-awesome](https://icons8.com/line-awesome).

This project is still under development!

## Dev Setup
```bash
#Yarn
yarn && yarn dev
#NPM
npm i && npm run dev
```

## Contributors
* Nicolai "Niggl" Ort
* Philipp Dormann